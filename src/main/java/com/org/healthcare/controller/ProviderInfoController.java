package com.org.healthcare.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.healthcare.entity.ProviderInfo;
import com.org.healthcare.service.ProviderInfoService;

@RestController
public class ProviderInfoController {

	@Autowired
	private ProviderInfoService providerInfoService;

	@RequestMapping("/findall")
	public List<ProviderInfo> findAll() {
		List<ProviderInfo> providerInfos = (List<ProviderInfo>) providerInfoService.findAll();
		return providerInfos;
	}

	@RequestMapping(value = "/providers", method = RequestMethod.GET)
	public List<ProviderInfo> searchAll(
			@RequestParam(value = "max_discharges", required = false, defaultValue = Double.MAX_VALUE
					+ "") Double maxDischarge,
			@RequestParam(value = "min_discharges", required = false, defaultValue = "0") Double minDischarges,
			@RequestParam(value = "max_average_covered_charges", required = false, defaultValue = Double.MAX_VALUE
					+ "") Double maxAverageCoveredCharges,
			@RequestParam(value = "min_average_covered_charges", required = false, defaultValue = "0") Double minAverageCoveredCharges,
			@RequestParam(value = "max_average_medicare_payments", required = false, defaultValue = Double.MAX_VALUE
					+ "") Double maxAverageMedicarePayments,
			@RequestParam(value = "min_average_medicare_payments", required = false, defaultValue = "0") Double minAverageMedicarePayments,
			@RequestParam(value = "state", required = false) String state) {

		List<ProviderInfo> providerInfos = (List<ProviderInfo>) providerInfoService.findBy(maxDischarge, minDischarges,
				maxAverageCoveredCharges, minAverageCoveredCharges, maxAverageMedicarePayments,
				minAverageMedicarePayments, state);
		
		return providerInfos;
	}
}
