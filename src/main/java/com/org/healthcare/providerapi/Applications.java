package com.org.healthcare.providerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@ComponentScan(basePackages = "com.org.healthcare")
@EnableJpaRepositories(basePackages = "com.org.healthcare.repository")
@EntityScan(basePackages = "com.org.healthcare.entity")
public class Applications {
	public static void main(String[] args) {
		SpringApplication.run(Applications.class, args);
	}
}
