package com.org.healthcare.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.org.healthcare.entity.ProviderInfo;

public interface ProviderInfoRepository extends JpaRepository<ProviderInfo, Long> {
	public List<ProviderInfo> findByState(String state);
}
