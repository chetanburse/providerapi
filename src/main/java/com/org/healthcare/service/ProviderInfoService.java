package com.org.healthcare.service;

import java.util.List;

import com.org.healthcare.entity.ProviderInfo;

public interface ProviderInfoService {

	List<ProviderInfo> findBy(Double maxDischarge, Double minDischarges, Double maxAverageCoveredCharges,
			Double minAverageCoveredCharges, Double maxAverageMedicarePayments, Double minAverageMedicarePayments,
			String state);

	List<ProviderInfo> findAll();
}
