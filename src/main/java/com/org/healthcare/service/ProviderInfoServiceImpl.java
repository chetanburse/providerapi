package com.org.healthcare.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.healthcare.entity.ProviderInfo;
import com.org.healthcare.repository.ProviderInfoRepository;

@Service
public class ProviderInfoServiceImpl implements ProviderInfoService {

	@Autowired
	private ProviderInfoRepository providerInfoRepository;

	public List<ProviderInfo> findAll() {
		List<ProviderInfo> providerInfos = (List<ProviderInfo>) providerInfoRepository.findAll();
		return providerInfos;
	}
	
	@Override
	public List<ProviderInfo> findBy(Double maxDischarge, Double minDischarges, Double maxAverageCoveredCharges,
			Double minAverageCoveredCharges, Double maxAverageMedicarePayments, Double minAverageMedicarePayments,
			String state) {

		List<ProviderInfo> infos = new ArrayList<>();
		ProviderInfo info = new ProviderInfo();
		info.setState(state);
		infos = (List<ProviderInfo>) providerInfoRepository.findByState(state);

		Stream<ProviderInfo> infoStream = infos.stream()
				.filter(p -> p.totalDischarged >= minDischarges && p.totalDischarged <= maxDischarge)
				.filter(p -> p.coveredCharges >= minAverageCoveredCharges
						&& p.coveredCharges <= maxAverageCoveredCharges)
				.filter(p -> p.medicarePayments >= minAverageMedicarePayments
						&& p.medicarePayments <= maxAverageMedicarePayments);

		infos = Arrays.asList(infoStream.toArray(ProviderInfo[]::new));

		return infos;
	}

}
