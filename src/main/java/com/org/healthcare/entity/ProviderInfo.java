package com.org.healthcare.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Entity
@Table(name="providerinfo")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Provider Name",
    "Provider Street Address",
    "Provider City",
    "Provider State",
    "Provider Zip Code",
    "Hospital Referral Region Description",
    "Total Discharges",
    "Average Covered Charges",
    "Average Total Payments",
    "Average Medicare Payments"
})
public class ProviderInfo implements Serializable {

	private static final long serialVersionUID = 2231854812676709064L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	public long id;
	
    @JsonProperty("Provider Name")
	@Column(name = "provider_name")
    public String providerName;

    @JsonProperty("Provider Street Address")
	@Column(name = "provider_street_address")
    public String providerAddress;
	
    @JsonProperty("Provider City")
	@Column(name = "provider_city")
    public String city;
	
    @JsonProperty("Provider State")
	@Column(name = "provider_state")
    public String state;
	
    @JsonProperty("Provider Zip Code")
	@Column(name = "provider_zip_code")
    public String zipCode;
	
    @JsonProperty("Hospital Referral Region Description")
	@Column(name = "hospital_referral_region_description")
    public String referralRegion;
    
    @JsonProperty("Total Discharges")
	@Column(name = "total_discharges")
    public Double totalDischarged;
	
    @JsonProperty("Average Covered Charges")
	@Column(name = "average_covered_charges")
    public Double coveredCharges;
	
    @JsonProperty("Average Medicare Payments")
	@Column(name = "average_medicare_payments")
    public Double medicarePayments;
	
	@JsonProperty("Average Total Payments")
	@Column(name = "average_total_payments")
	public Double totalPayments;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderAddress() {
		return providerAddress;
	}

	public void setProviderAddress(String providerAddress) {
		this.providerAddress = providerAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getReferralRegion() {
		return referralRegion;
	}

	public void setReferralRegion(String referralRegion) {
		this.referralRegion = referralRegion;
	}

	public String getTotalDischarged() {
		return "$" + totalDischarged;
	}

	public String getCoveredCharges() {
		return "$" + coveredCharges;
	}

	public String getMedicarePayments() {
		return "$" + medicarePayments;
	}

	public String getTotalPayments() {
		return "$" + totalPayments;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setTotalDischarged(Double totalDischarged) {
		this.totalDischarged = totalDischarged;
	}

	public void setCoveredCharges(Double coveredCharges) {
		this.coveredCharges = coveredCharges;
	}

	public void setMedicarePayments(Double medicarePayments) {
		this.medicarePayments = medicarePayments;
	}

	public void setTotalPayments(Double totalPayments) {
		this.totalPayments = totalPayments;
	}	

	
	
}

