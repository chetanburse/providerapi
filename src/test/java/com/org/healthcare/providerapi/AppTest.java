package com.org.healthcare.providerapi;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.org.healthcare.service.ProviderInfoService;

import com.org.healthcare.entity.ProviderInfo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Applications.class)
public class AppTest  {
	
	@Autowired
	private ProviderInfoService ProviderInfoService;

	@Test
	public void testProviderApi() {
		List<ProviderInfo> i = ProviderInfoService.findBy(50d, 6d, 10000d, 2000d, 6000d, 1000d, "AL");
		assertTrue(i.size() == 391);
	}
	
	@Test
	public void testNoParams() {
		List<ProviderInfo> i = ProviderInfoService.findBy(null, null, null, null, null, null, null);
		assertTrue(i.size() == 0);
	}
	
	@Test
	public void testOnlyStateParam() {
		List<ProviderInfo> i = ProviderInfoService.findBy(Double.MAX_VALUE, 0d, Double.MAX_VALUE, 0d, Double.MAX_VALUE, 0d, "AL");
		boolean flag = true;
		for(ProviderInfo p : i) {
			if(!p.getState().equals("AL")) {
				flag=false;
				break;
			}
		}
		assertTrue(flag);
	}
}
